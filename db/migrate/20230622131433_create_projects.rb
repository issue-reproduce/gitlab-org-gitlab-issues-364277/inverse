class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.references :project_namespace, index: true, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end

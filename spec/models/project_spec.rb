require 'rails_helper'

RSpec.describe Project, type: :model do
  it 'triggers update callbacks only once when child is saved with unsaved parent' do
    expect_any_instance_of(ProjectNamespace).to receive(:project_namespace_before_save).and_call_original
    expect_any_instance_of(described_class).to receive(:project_before_save).and_call_original  # Fails

    Project.create!(project_namespace: ProjectNamespace.new)
  end

  it 'triggers update callbacks only once when parent is saved' do # Passes
    expect_any_instance_of(ProjectNamespace).to receive(:project_namespace_before_save).and_call_original
    expect_any_instance_of(described_class).to receive(:project_before_save).and_call_original

    ProjectNamespace.create!(project: Project.new)
  end
end

# README

This reproduces triggering of validations and callbacks twice when the child (Project) is saved before the
parent (ProjectNamespace). This is related to https://gitlab.com/gitlab-org/gitlab/-/issues/364277 where
introducing inverse_of causes project to get saved twice.

You can simply run the specs using `bundle exec rspec` to reproduce the issue or follow the below steps:

### Child is saved before parent

Run `bin/rails c`:

```ruby
project = Project.new
#<Project:0x000000010a6ae4c8 id: nil, project_namespace_id: nil, created_at: nil, updated_at: nil>
project_namespace = project.build_project_namespace
#<ProjectNamespace:0x000000010a77e970 id: nil, created_at: nil, updated_at: nil>
project.save!
```

Output:
```
project_validation
project_namespace_validation
project_namespace_before_save
  TRANSACTION (0.1ms)  begin transaction
  ProjectNamespace Create (0.8ms)  INSERT INTO "project_namespaces" ("created_at", "updated_at") VALUES (?, ?)  [["created_at", "2023-06-22 13:42:46.167907"], ["updated_at", "2023-06-22 13:42:46.167907"]]
project_validation
project_before_save
  Project Create (0.1ms)  INSERT INTO "projects" ("project_namespace_id", "created_at", "updated_at") VALUES (?, ?, ?)  [["project_namespace_id", 3], ["created_at", "2023-06-22 13:42:46.172356"], ["updated_at", "2023-06-22 13:42:46.172356"]]
project_after_save
project_namespace_after_save
project_before_save
project_after_save
```

### Parent is saved before child

```ruby
project = Project.new
#<Project:0x000000011154c718 id: nil, project_namespace_id: nil, created_at: nil, updated_at: nil>
project_namespace = project.build_project_namespace
#<ProjectNamespace:0x000000011171cb10 id: nil, created_at: nil, updated_at: nil>
project_namespace.save!
```

Output:
```
project_namespace_validation
project_namespace_before_save
  TRANSACTION (0.1ms)  begin transaction
  ProjectNamespace Create (0.7ms)  INSERT INTO "project_namespaces" ("created_at", "updated_at") VALUES (?, ?)  [["created_at", "2023-06-22 13:55:54.332005"], ["updated_at", "2023-06-22 13:55:54.332005"]]
project_validation
project_before_save
  Project Create (0.1ms)  INSERT INTO "projects" ("project_namespace_id", "created_at", "updated_at") VALUES (?, ?, ?)  [["project_namespace_id", 4], ["created_at", "2023-06-22 13:55:54.335694"], ["updated_at", "2023-06-22 13:55:54.335694"]]
project_after_save
project_namespace_after_save
```
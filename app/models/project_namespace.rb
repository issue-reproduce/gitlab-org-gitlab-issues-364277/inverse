class ProjectNamespace < ApplicationRecord
  has_one :project, inverse_of: :project_namespace

  validate :project_namespace_validation
  before_save :project_namespace_before_save
  after_save :project_namespace_after_save

  def project_namespace_validation
    puts __method__
  end

  def project_namespace_before_save
    puts __method__
  end

  def project_namespace_after_save
    puts __method__
  end
end

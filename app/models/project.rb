class Project < ApplicationRecord
  belongs_to :project_namespace, inverse_of: :project

  validate :project_validation
  before_save :project_before_save
  after_save :project_after_save

  def project_validation
    puts __method__
  end

  def project_before_save
    puts __method__
  end

  def project_after_save
    puts __method__
  end
end
